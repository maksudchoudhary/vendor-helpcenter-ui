package constants;

public class Constants {


    public static final String ORANGE_COLOR_HEXA = "rgba(252, 128, 26, 1)";
    public static final String[] ISSUE_LEVEL_NAMES = {"Payment/Finance", "Menu", "Live Orders", "Restaurant Open/Close Status"};
    public static final String HOME_QUESTION_TEXT = "Please Select What You Need Help With";
    public static final String HOMEPAGE_HELPCENTRE_TEXT = "help center";
    public static final String TICKET_HOMEPAGE_VALUE = "View Tickets";
    public static final String HOMEPAGE_VIEW_TICKETS = "View Tickets";
    public static final String TEXT_COLOR = "color";
    public static final String SRC = "src";
    public static final String HOMEPAGE_HEADER_QUESTIONTEXT_MESSAGE = "what do you need help with is not present on homepage";
    public static final String HOMEPAGE_VIEW_TICKETS_MESSAGE = "Home page view tickets text is not present";
    public static final String HOMEPAGE_HEADERTEXT_MESSAGE = "Not Landed on Homepage";
    public static final String HOMEPAGE_IMG_MESSAGE ="Images is not displayed";
    public static final String TEXT_COLOR_MESSAGE = "Color doesnt match after clicking it means not clickable";
    public static final String LEVEL_NAMES_MESSAGE = "Level names not matched";
    public static final String HOME_QUESTION_TEXT_MESSAGE ="Not navigated to home after clicking on view help all help topics";
    public static final String VIEW_TICKETS_TEXT_MESSAGE = "Not navigated to tickets page";
    public static final String SPLIT = "₹";
    public static final String RESTAURANT_OPEN_CLOSE_TEXT = "Restauant Open/Close Status";
    public static final String LIVE_ORDERS_TEXT = "live orders";
    public static final String LIVE_ORDERS_OUTLET_TEXT ="Select an order to proceed";
    public static final String ORDER_DETAILS_HEADER_TEXT = "order details";
    public static final String CUSTOMER_SUGGESTION_TEXT = "“I need bill from restaurant for myself. Please don’t forget.. Please!”";
    public static final String CALCULATION_MISMATCH_MESSAGE = "There is some issue in calculation";
    public static final String COUPON_DISCOUNT_TEXT = "Coupon discount";
}
