package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import swiggybase.Swiggy_Base;

import java.util.List;

public class SwiggyHelpCentreHomepage extends Swiggy_Base {

    public By tickets_helpcenter =By.xpath("//*[contains(text(),'Help Center')]");
    public By homepage_header_question =By.xpath("//*[contains(@class,'Controller_titleContainer')][1]");
    public By view_tickets  = By.xpath("//*[contains(@class,'Button_primary')]");
    public By homepage_header_text = By.xpath("//*[contains(@class,'Header_titleXL')]");
    public By homepage_header_img = By.xpath("//*[contains(@class,'QueryList_solMock')]/img");
    public By level_one_texts = By.xpath("//*[contains(@class,'QueryList_issueItem')]//div[@data-testid='issue-title']");
    public By level_second = By.xpath("//*[(contains(@class,'QueryList_issueItem')) and not(contains(@class,'QueryList_last'))]//descendant::div[@data-testid='issue-title']");
    public By cancel_button = By.xpath("//*[text()='Cancel']");
    public By level_one_descendant_text= By.xpath("//descendant::div[@data-testid='issue-title']");
    public By view_help_topic = By.xpath("//*[contains(@class,'Breadcrumb_container')]//div[(contains(@class,'ViewAllButton_buttonText'))]");
    public By view_tickets_text = By.xpath("//*[contains(@class,'Header_titleContainer')]/div[1]");
    public By first_level_complete_traverse = By.xpath("//*[contains(@class,'QueryList_querylistContainer')][1]/div[(contains(@class,'QueryList_issueItem'))]//div[@data-testid='issue-title']");
    public By second_level_complete_traverse = By.xpath("//*[contains(@class,'QueryList_querylistContainer')][2]/div[(contains(@class,'QueryList_issueItem'))]//div[@data-testid='issue-title']");
    public By third_level_complete_traverse = By.xpath("//*[contains(@class,'QueryList_querylistContainer')][3]/div[(contains(@class,'QueryList_issueItem'))]//div[@data-testid='issue-title']");
    public By breadcrumb_level_two = By.xpath("//span[@data-testid='issue-name'][2]");
    public By breadcrumb_level_one = By.xpath("//*[contains(@class,'Breadcrumb_textDisp')]/div[contains(@class,'Breadcrumb_headerWeb')]/span[1]");
    public By live_order_level1_click = By.xpath("//*[contains(text(),'Live order')]");
    public By outlet_selector = By.xpath("//*[contains(@class,'AccordionItem_accordionItemLine')]");
    public By outlet_opened = By.xpath("//*[contains(@class,'AccordionItem_accordionItemOpened')]");
    public By outlet_open_select_area = By.xpath("//*[contains(@class,'OutletPicker_selectOutlet')]");
    public By outlet_open_select_area_multiple = By.xpath("//*[contains(@class,'AccordionItem_accordionItemOpened')]//div[contains(@class,'OutletPicker_selectOutlet')]");
    public By outlet_level2_liveorder_text = By.xpath("//*[contains(@class,'OrderPicker_webHeader')]");
    public By live_orders_count = By.xpath("//*[contains(@data-testid,'order-list-container')]");
    public By live_orders_click = By.xpath("//*[contains(@data-testid,'order-list-container')]");
    public By order_details_header_text = By.xpath("//*[contains(@class,'OrderSelector_webHeader')]");
    public By customer_suggestion_text = By.xpath("//*[contains(@class,'OrderSelector_customerdetails')]");
    public By bill_text = By.xpath("//*[contains(@class,'FoodBill_subcontainer')]//following::div//following-sibling::div[contains(@class,'FoodBill_container2')]");
    public By expected_total_text = By.xpath("//*[contains(@class,'FoodBill_container1')]/div[contains(@class,'FoodBill_subcontainer2')]");
    public By order_details_submit = By.xpath("//*[contains(@class,'OrderSelector_submit')]");
    public By frame = By.id("iframeHelpCenter");

    public SwiggyHelpCentreHomepage(){

        PageFactory.initElements(driver,this);
    }

    public WebElement ticketsHelpcenter(){
        return driver.findElement(tickets_helpcenter);
    }

    public WebElement validateHompageHeaderQuestion(){
        return driver.findElement(homepage_header_question);
    }

    public WebElement validateViewTickets(){

        return driver.findElement(view_tickets);
    }

    public WebElement validatHomepageHeaderText(){

        return driver.findElement(homepage_header_text);
    }

    public WebElement validateHomepageimg(){

        return driver.findElement(homepage_header_img);
    }

    public WebElement validateLevelOne(){
        return driver.findElement(level_one_texts);
    }

    public WebElement validateLevelSecond(){
        return driver.findElement(level_second);
    }

    public WebElement cancelbutton(){
        return driver.findElement(cancel_button);
    }

    public WebElement level_one_descendant_text(){
        return driver.findElement(level_one_descendant_text);
    }

    public WebElement view_help_topic(){
        return driver.findElement(view_help_topic);
    }

    public WebElement view_tickets_text(){
        return driver.findElement(view_tickets_text);
    }

    public WebElement first_level_complete_traverse(){
        return driver.findElement(first_level_complete_traverse);
    }

    public WebElement second_level_complete_traverse(){
        return driver.findElement(second_level_complete_traverse);
    }

    public WebElement third_level_complete_traverse(){
        return driver.findElement(third_level_complete_traverse);
    }

    public WebElement breadcrumb_click_level_two(){
        return driver.findElement(breadcrumb_level_two);
    }

    public WebElement breadcrumb_click_level_one(){
        return driver.findElement(breadcrumb_level_one);
    }

    public List<WebElement> outlet_selector_click(){
        return driver.findElements(outlet_selector);
    }

    public WebElement live_order_level1_click(){
        return driver.findElement(live_order_level1_click);
    }

    public WebElement outlet_opened_validator(){
        return driver.findElement(outlet_opened);
    }

    public List<WebElement> outlet_area_selector_multiple(){
        return driver.findElements(outlet_open_select_area_multiple);

    }

    public WebElement outlet_area_selector(){
        return driver.findElement(outlet_open_select_area);

    }


    public WebElement outlet_level2_liveorder_text(){
        return driver.findElement(outlet_level2_liveorder_text);
    }

    public List<WebElement> outlet_live_orders_count(){
        return driver.findElements(live_orders_count);
    }

    public WebElement outlet_live_orders_click(){
        return driver.findElement(live_orders_click);
    }

    public WebElement order_details_header(){
        return driver.findElement(order_details_header_text);
    }

    public WebElement customer_suggestion_text(){
        return driver.findElement(customer_suggestion_text);
    }

    public List<WebElement> bill_text_calculation(){
        return driver.findElements(bill_text);
    }

    public WebElement expected_total_text(){
        return driver.findElement(expected_total_text);
    }

    public WebElement order_details_submit(){
        return driver.findElement(order_details_submit);
    }

    public WebElement frame(){
        return driver.findElement(frame);
    }
}
