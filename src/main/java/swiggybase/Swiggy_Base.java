package swiggybase;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.FirefoxDriver;
import pageobjects.SwiggyHelpCentreHomepage;
import utils.WebDriverUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.io.FileInputStream;

public class Swiggy_Base {


    public static WebDriver driver;
    public static Properties prop;
    static SwiggyHelpCentreHomepage swiggyhome= new SwiggyHelpCentreHomepage();

    public Swiggy_Base(){
        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+ "/src/main/resources/config-"+System.getenv("environment")+".properties");
            prop.load(ip);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void helpcentre_initialization(){
        String browserName = prop.getProperty("browser");

        if(browserName.equalsIgnoreCase("chrome")){
            //Create a map to store  preferences
            Map<String, Object> prefs = new HashMap<String, Object>();

            //add key and value to map as follow to switch off browser notification
            //Pass the argument 1 to allow and 2 to block
            prefs.put("profile.default_content_setting_values.notifications", 2);
            ChromeOptions options = new ChromeOptions();
            System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
            //     options.addExtensions(new File(prop.getProperty("extension_path")));
 //           WebDriverManager.chromedriver().setup();
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--test-type");
            options.addArguments("--disable-gpu");
            options.addArguments("--disable-extensions");
            options.addArguments("--proxy-server='direct://'");
            options.addArguments("--proxy-bypass-list=*");
            options.addArguments("--no-first-run");
            options.addArguments("--no-default-browser-check");
            options.addArguments("--ignore-certificate-errors");
            options.setExperimentalOption("useAutomationExtension", false);
            options.setExperimentalOption("prefs", prefs);
//            options.setBinary("/usr/local/bin/chromedriver");
            options.addArguments("--headless");
            options.addArguments("--start-maximized");
            options.addArguments("--window-size=1900,1080");
            driver = new ChromeDriver(options);

        }
        else if(browserName.equals("FF")){
            FirefoxProfile profile = new FirefoxProfile();
            profile.addExtension(new File(prop.getProperty("extension_path")));
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setProfile(profile);
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver(firefoxOptions);
        }

//        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get(prop.getProperty("application_url"));
        WebDriverUtils.startExplicitlyWait(driver);
        driver.findElement(By.cssSelector("[name='Mob']")).sendKeys(prop.getProperty("username"));
        driver.findElement(By.xpath("//*[@type='password']")).sendKeys(prop.getProperty("password"));
        driver.findElement(By.id("sign-up-btn")).click();
    //    WebDriverUtils.pop_up();
        driver.findElement(By.xpath("//*[@title='Help']")).click();
        WebDriverUtils.triggerThreadSleep(5000);
        WebDriverUtils.switchtoframe(driver,swiggyhome.frame());

    }

    public static void takeScreenshotAtEndOfTest() throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String currentDir = System.getProperty("user.dir");
        FileUtils.copyFile(scrFile, new File(currentDir + "/screenshots/" + System.currentTimeMillis() + ".png"));
    }

}
