package utils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import pageobjects.SwiggyHelpCentreHomepage;
import swiggybase.Swiggy_Base;

import java.util.*;
import java.util.concurrent.TimeUnit;


public class WebDriverUtils extends Swiggy_Base {

    public static SwiggyHelpCentreHomepage swiggyhome = new SwiggyHelpCentreHomepage();
    private static int driverTimeout = 20;
    public static Logger log = LogManager.getLogger(WebDriverUtils.class.getName());

    public WebDriverUtils(){
        super();
    }

    public static int getDriverTimeout() {
        return driverTimeout;
    }

    public static String[] split(WebElement element, String valuetosplit) {
        String value = element.getText();
        return value.split(valuetosplit);
    }
    public static boolean checkForContains(List<String> list, String textToBeVerified) {
        String textToBeVerifiedLowerCase = textToBeVerified.toLowerCase();
        for (String actual : list) {
            if (actual.toLowerCase().contains(textToBeVerifiedLowerCase)) {
                return true;
            }
        }
        return false;
    }

    public static void triggerThreadSleep(int milliSec) {
        try {
            Thread.sleep(milliSec);
        } catch (InterruptedException e) {
        }
    }
    public static void cancel_button(){
        try {
            if (swiggyhome.cancelbutton() != null) {
               triggerThreadSleep(1000);
                swiggyhome.cancelbutton().click();
            }
        } catch (Exception e) {
            log.info("Cancel button is still not visible : "+ e.getMessage());
        }
    }



    public static void quit() {
        Reporter.log("Current Thread Name :" + Thread.currentThread().getName() + ":" + Thread.currentThread().getId(), true);
        try {
            driver.quit();
        } catch (Throwable t) {
            log.info(t.getMessage());
        }
    }


    public static void switchtoframe(WebDriver driver,WebElement byElement){
        driver.switchTo().frame(byElement);
    }

    public static List<WebElement> webdriverwait(By byElement){
       WebDriverWait wait = new WebDriverWait(driver,20);
       return  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byElement));
    }

    public static void pop_up(){

        try{
            if(driver.findElement(By.xpath("//*[@class='container-fluid']/div[@class='row']"))!=null){
                driver.findElement(By.className("swal2-close")).click();
            }
        }catch(Exception e){
            log.info("Pop-up is not available"+e.getMessage());
        }
    }

    public static void navigateback(){
        driver.navigate().back();
    }


    public static void startExplicitlyWait(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, getDriverTimeout());
        driver.manage().timeouts().implicitlyWait(getDriverTimeout(), TimeUnit.SECONDS);

    }

}
