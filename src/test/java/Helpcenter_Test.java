import constants.Constants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageobjects.SwiggyHelpCentreHomepage;
import swiggybase.Swiggy_Base;
import utils.WebDriverUtils;


public class Helpcenter_Test extends Swiggy_Base {
    SwiggyHelpCentreHomepage swiggyhome;
    public static Logger log = LogManager.getLogger(Helpcenter_Test.class.getName());

    public Helpcenter_Test() {
        super();
    }

    @BeforeClass
    public void setup() {
        helpcentre_initialization();
        swiggyhome = new SwiggyHelpCentreHomepage();
    }

    @Test(priority = 0, description = "Image and homepage verification", enabled = true)
    public void test_image_verify() {
        Assert.assertEquals(swiggyhome.validateHompageHeaderQuestion().getText(), Constants.HOME_QUESTION_TEXT, Constants.HOMEPAGE_HEADER_QUESTIONTEXT_MESSAGE);
        Assert.assertEquals(swiggyhome.validateViewTickets().getText(), Constants.HOMEPAGE_VIEW_TICKETS, Constants.HOMEPAGE_VIEW_TICKETS_MESSAGE);
        Assert.assertEquals(swiggyhome.validatHomepageHeaderText().getText().toLowerCase(), Constants.HOMEPAGE_HELPCENTRE_TEXT, Constants.HOMEPAGE_HEADERTEXT_MESSAGE);
        //     Assert.assertNotNull(swiggyhome.validateHomepageimg().getAttribute(Constants.SRC), Constants.HOMEPAGE_IMG_MESSAGE);


    }

    @Test(priority = 1, description = "Test First level", enabled = true)
    public void test_firstnodelevel() throws InterruptedException {
        LevelValidation.first_level_validation();

    }

    @Test(priority = 2, description = "Tickets", enabled = true)
    public void test_tickets() throws InterruptedException {

        swiggyhome.validateViewTickets().click();
        WebDriverUtils.triggerThreadSleep(5000);
        log.info(swiggyhome.view_tickets_text().getText());
        Assert.assertEquals(swiggyhome.view_tickets_text().getText(), Constants.TICKET_HOMEPAGE_VALUE.toUpperCase(), Constants.VIEW_TICKETS_TEXT_MESSAGE);
        swiggyhome.ticketsHelpcenter().click();
        WebDriverUtils.triggerThreadSleep(5000);
        //      WebDriverUtils.switchtoframe(driver,swiggyhome.frame());
        test_image_verify();
    }

    @Test(priority = 3, description = "Each_level_Test", enabled = true)
    public void testing_each_level() throws InterruptedException {
        LevelValidation.each_level_validating();

    }

    @Test(priority = 4, description = "Live_Orders_Test", enabled = true)
    public void Live_order(){
        swiggyhome.live_order_level1_click().click();
        LevelValidation.outlet_selection_validation();
        LevelValidation.live_order_outlet_level_validation();
    }

    @AfterClass(description = "Quitting the browser")
    public void teardown() {

        WebDriverUtils.quit();
    }


}