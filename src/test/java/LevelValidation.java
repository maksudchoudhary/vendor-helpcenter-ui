import constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.SwiggyHelpCentreHomepage;
import swiggybase.Swiggy_Base;
import utils.WebDriverUtils;

import java.util.Arrays;
import java.util.List;

public class LevelValidation extends Swiggy_Base{

    static SwiggyHelpCentreHomepage swiggyhome = new SwiggyHelpCentreHomepage();;
    public static Logger log = LogManager.getLogger(LevelValidation.class.getName());

    public static void first_level_validation(){
        List<WebElement> element = WebDriverUtils.webdriverwait(swiggyhome.level_one_texts);
        log.info("Total number of options available in first level : " + element.size());
        for (int i = 0; i < element.size(); i++) {

            try {
                if ((swiggyhome.validateLevelSecond()) != null) {
                    WebDriverUtils.triggerThreadSleep(1000);
                    log.info("Clicking on ..... " + element.get(i).getText());
                    log.info("Color before clicking : " + element.get(i).getCssValue(Constants.TEXT_COLOR));
                    element.get(i).click();
                    log.info("Color after clicking : " + element.get(i).getCssValue(Constants.TEXT_COLOR));
//                    Assert.assertEquals(element.get(i).getCssValue(Constants.TEXT_COLOR), Constants.ORANGE_COLOR_HEXA, Constants.TEXT_COLOR_MESSAGE);
                    try {
                        if (swiggyhome.cancelbutton() != null) {
                            WebDriverUtils.triggerThreadSleep(1000);
                            swiggyhome.cancelbutton().click();
                        }
                    } catch (Exception e) {
                        log.info("Cancel button is still not visible");
                        Assert.assertEquals(element.get(i).getCssValue(Constants.TEXT_COLOR), Constants.ORANGE_COLOR_HEXA, Constants.TEXT_COLOR_MESSAGE);
                    }
                    //                   Assert.assertTrue(Utils.checkForContains(Arrays.asList(Constants.ISSUE_LEVEL_NAMES), element.get(i).findElement((swiggyhome.level_one_descendant_text)).getText()), Constants.LEVEL_NAMES_MESSAGE);

                }
            } catch (Exception e) {

                WebDriverUtils.triggerThreadSleep(3000);
                log.info("Clicking on ..... " + element.get(i).findElement(swiggyhome.level_one_descendant_text).getText());
                log.info("Color before clicking : " + element.get(i).getCssValue(Constants.TEXT_COLOR));
                element.get(i).click();
                log.info("Color after clicking : " + element.get(i).getCssValue(Constants.TEXT_COLOR));
                Assert.assertEquals(element.get(i).getCssValue(Constants.TEXT_COLOR), Constants.ORANGE_COLOR_HEXA, Constants.TEXT_COLOR_MESSAGE);
                Assert.assertTrue(WebDriverUtils.checkForContains(Arrays.asList(Constants.ISSUE_LEVEL_NAMES), element.get(i).findElement(swiggyhome.level_one_descendant_text).getText()), Constants.LEVEL_NAMES_MESSAGE);
            }

        }

    }

    public static void outlet_selection_validation(){
        List<WebElement> list = swiggyhome.outlet_selector_click();
        log.info("Total number of outlets available : "+ list.size());
        for(int i=0; i<list.size();i++){
            log.info("Before selecting outlet : "+list.get(i).getText());
            System.out.println("Before selecting outlet : "+list.get(i).getText());
            try {
                if(swiggyhome.outlet_opened_validator() == null){
                    list.get(i).click();
                }
                if (swiggyhome.outlet_opened_validator() != null) {
                    log.info("After selecting outlet : " + list.get(i).getText());
                }
            }catch(Exception e){
                Assert.assertFalse(Boolean.parseBoolean("outlet selecting is not working : "+e.getMessage()));
            }
        }
    }

    public static void live_order_outlet_level_validation(){
        double items_total=0;
        double sum=0;
        double coupon_value = 0;
        int count = 0;
        List<WebElement> list = swiggyhome.outlet_selector_click();
        for(int i=0; i<list.size();i++){
            count++;
            log.info("Before selecting outlet : "+list.get(i).getText());
            if(count>1){
                list.get(i).click();
            }
            try {
                if(swiggyhome.outlet_opened_validator() == null){
                    list.get(i).click();
                }
                if (swiggyhome.outlet_opened_validator() != null) {
                    log.info("After selecting outlet : " + list.get(i).getText());
                    List<WebElement> list_restaurants = swiggyhome.outlet_area_selector_multiple();
                    for(int j=0;j<list_restaurants.size();j++) {
                        list_restaurants.get(j).click();
                        Assert.assertTrue(swiggyhome.outlet_level2_liveorder_text().getText().equalsIgnoreCase(Constants.LIVE_ORDERS_OUTLET_TEXT));
                        List<WebElement> live_orders_count = swiggyhome.outlet_live_orders_count();
                        log.info("Total number of live orders are : " + live_orders_count.size());
                        if(!live_orders_count.isEmpty()) {
                            swiggyhome.outlet_live_orders_click().click();
                            Assert.assertTrue(swiggyhome.order_details_header().getText().equalsIgnoreCase(Constants.ORDER_DETAILS_HEADER_TEXT),"Header details for order didn't matched");
                            try {
                                Assert.assertTrue(swiggyhome.customer_suggestion_text().getText().equalsIgnoreCase(Constants.CUSTOMER_SUGGESTION_TEXT), "Customer suggestion text area is not available");
                            }catch(Exception e){
                                log.info("Customer suggestion text area is not available"+e.getMessage());
                            }

                            List<WebElement> number_of_items_price = driver.findElements(By.xpath("//*[contains(@class,'FoodItem_itemPrice')]/div"));
                            log.info("Number of items ordered are : " + number_of_items_price.size());
                            for (int items = 0; items < number_of_items_price.size(); items++)
                            {
                                String[] val = WebDriverUtils.split(number_of_items_price.get(items), Constants.SPLIT);
                                double value = Double.parseDouble(val[1]);
                                items_total = items_total + value;

                            }
                            List<WebElement> bill = swiggyhome.bill_text_calculation();
                            log.info("Number of fields in the bill are : " + bill.size());
                            for (int k = 0; k < bill.size(); k++) {
                                log.info("Number of fields with field name and amount : " + bill.get(k).getText());
                                if (bill.get(k).getText().contains(Constants.COUPON_DISCOUNT_TEXT)) {
                                    String[] val = WebDriverUtils.split(bill.get(k), Constants.SPLIT);
                                    coupon_value = Double.parseDouble(val[1]);
                                } else {
                                    String[] val = WebDriverUtils.split(bill.get(k), Constants.SPLIT);
                                    double value = Double.parseDouble(val[1]);
                                    sum = items_total + value;
                                }

                            }

                            Assert.assertEquals(sum - coupon_value, Double.parseDouble(swiggyhome.expected_total_text().getText().split(Constants.SPLIT)[1]), Constants.CALCULATION_MISMATCH_MESSAGE);
                            swiggyhome.order_details_submit().click();
                            break;
                        }
                    }
                }

            }catch(Exception e){
                Assert.assertFalse(Boolean.parseBoolean("outlet selecting is not working or there is no orders present in outlet : "+e.getMessage()));
            }
        }
        WebDriverUtils.cancel_button();
    }


    public static void level2_issue_check() {
        List<WebElement> level2 = WebDriverUtils.webdriverwait(swiggyhome.second_level_complete_traverse);
        System.out.println("Total number of options available in first level : " + level2.size());
        for (int j = 0; j < level2.size(); j++) {
            try {
                if (level2.get(j).getText().isEmpty()) {
                    swiggyhome.breadcrumb_click_level_two().click();
                }
            } catch (Exception e) {
                log.info("Level two is visible :" + e.getMessage());
            }
            log.info("Clicking on ..... " + level2.get(j).getText());
            log.info("Color before clicking : " + level2.get(j).getCssValue(Constants.TEXT_COLOR));
            WebDriverUtils.triggerThreadSleep(1000);
            level2.get(j).click();
            log.info("Color after clicking : " + level2.get(j).getCssValue(Constants.TEXT_COLOR));
            WebDriverUtils.cancel_button();
        }
    }

    public static void level3_issue_check(){
        List<WebElement> level3 = WebDriverUtils.webdriverwait(swiggyhome.third_level_complete_traverse);
        log.info("Total number of options available in third level : " + level3.size());
        for (int k = 0; k < level3.size(); k++) {
            log.info("Clicking on ..... " + level3.get(k).getText());
            log.info("Color before clicking : " + level3.get(k).getCssValue(Constants.TEXT_COLOR));
            WebDriverUtils.triggerThreadSleep(2000);
            level3.get(k).click();
            log.info("Color after clicking : " + level3.get(k).getCssValue(Constants.TEXT_COLOR));

        }
        WebDriverUtils.cancel_button();

    }

    public static void level2_level3_check(){
        try {
            List<WebElement> level2 = WebDriverUtils.webdriverwait(swiggyhome.second_level_complete_traverse);
            //             List<WebElement> element2 = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(l2));
            log.info("Total number of options available in second level : " + level2.size());
            for (int j = 0; j < level2.size(); j++) {
                try {
                    if (level2.get(j).getText().isEmpty()) {
                        swiggyhome.breadcrumb_click_level_two().click();
                    }
                } catch (Exception e) {
                    log.info("Level two is visible :" + e.getMessage());
                }
                log.info("Clicking on ..... " + level2.get(j).getText());
                log.info("Color before clicking : " + level2.get(j).getCssValue(Constants.TEXT_COLOR));
                WebDriverUtils.triggerThreadSleep(1000);
                level2.get(j).click();
                log.info("Color after clicking : " + level2.get(j).getCssValue(Constants.TEXT_COLOR));
                WebDriverUtils.cancel_button();
            }
        }catch(Exception ae){
            log.info("level 2 is not available");
        }
        try {
            level3_issue_check();
        }catch(Exception ae){
            log.info("level 3 is not available");
        }

    }


    public static void each_level_validating(){
        List<WebElement> level1 = WebDriverUtils.webdriverwait(swiggyhome.first_level_complete_traverse);
        log.info("Total number of options available in first level : " + level1.size());
        for (int i = 0; i < level1.size(); i++) {
            try{
                if(level1.get(i).getText().isEmpty()){
                    swiggyhome.breadcrumb_click_level_one().click();
                }
            } catch(StaleElementReferenceException se){
                swiggyhome.breadcrumb_click_level_one().click();
                level1 = WebDriverUtils.webdriverwait(swiggyhome.first_level_complete_traverse);
                //    break;
            } catch(Exception e){
                log.info("Level two is visible :"+e.getMessage());
            }
            WebDriverUtils.triggerThreadSleep(2000);
            log.info("Clicking on ..... " + level1.get(i).getAttribute("innerText"));
            log.info("Color before clicking : " + level1.get(i).getCssValue(Constants.TEXT_COLOR));
            level1.get(i).click();
            log.info("Color after clicking : " + level1.get(i).getCssValue(Constants.TEXT_COLOR));
            try {
                if (swiggyhome.cancelbutton() != null) {
                    WebDriverUtils.triggerThreadSleep(1000);
                    //                   if(level1.get(i).getText().equalsIgnoreCase(Constants.RESTAURANT_OPEN_CLOSE_TEXT)){
                    List<WebElement> list = swiggyhome.outlet_selector_click();
                    for(int j=0; j<list.size();j++) {
                        log.info("Before selecting outlet : " + list.get(j).getText());

                        try {
                            if(swiggyhome.outlet_opened_validator() == null){
                                list.get(j).click();
                            }
                            if (swiggyhome.outlet_opened_validator() != null) {
                                log.info("After selecting outlet : " + list.get(j).getText());
                                swiggyhome.outlet_area_selector().click();
                                try {
                                    if (driver.findElement(By.xpath("//*[contains(@class,'OrderPicker_webHeader')]")).getText().contains("Select an order to proceed")) {
                                        live_order_outlet_level_validation();
                                    }
                                }catch(Exception e){
                                    log.info("Still not on the live order outlet page"+e.getMessage());
                                }
                            }
                        } catch (Exception e) {
                            Assert.assertFalse(Boolean.parseBoolean("outlet selecting is not working : " + e.getMessage()));
                        }
                    }

                    //                   }else {

                    //      swiggyhome.cancelbutton().click();
                }
                //               }
            } catch (Exception e) {
                log.info("Cancel button is still not visible :" + e.getMessage());
            }
            level2_level3_check();

        }
    }
}
